import math
from flask import Flask, request, jsonify

app = Flask(__name__)

def numerical_integration(lower, upper, N):
    total_area = 0
    width = (upper - lower) / N
    
    for i in range(N):
        x = lower + (i + 0.5) * width
        area = abs(math.sin(x)) * width
        total_area += area
    
    return total_area

@app.route('/numericalintegralservice/<lower>/<upper>', methods=['GET'])
def numerical_integral(lower, upper):
    lower = float(lower)
    upper = float(upper)
    results = []
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        results.append(numerical_integration(lower, upper, N))
    return jsonify(results)

if __name__ == '__main__':
    app.run(debug=True)