# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

'''
It receives a tuple in the form (key, value)
'''
def main(params: str) -> str:
    value = params[1]
    words = value.split()
    mapped_output = [(words[i],1) for i in range(len(words))]
    return mapped_output

