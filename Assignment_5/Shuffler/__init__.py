# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

'''
It takes as input a list [(key, value), ..., ...]
Return: dictionary key, [list of values]
'''
def main(params: str) -> str:
    dict_output = {}
    for tuple in params:
        if tuple[0] not in dict_output:
            dict_output[tuple[0]] = []
        dict_output[tuple[0]].append(tuple[1])
    return dict_output
