# This function is not intended to be invoked directly. Instead it will be
# triggered by an HTTP starter function.
# Before running this sample, please:
# - create a Durable activity function (default name is "Hello")
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging
import json

import azure.functions as func
import azure.durable_functions as df


def orchestrator_function(context: df.DurableOrchestrationContext):
    lines = yield context.call_activity('GetInputDataFn', None)
    map_tasks = []
    for line in lines:
        map_tasks.append(context.call_activity('Mapper', line))
    map_results = yield context.task_all(map_tasks)
    # Flatten the list before sending it to shuffler
    map_results = sum(map_results, [])

    shuffle_results = yield context.call_activity('Shuffler', map_results)
    reduce_tasks = []
    for key in shuffle_results:
        reduce_tasks.append(context.call_activity('Reducer', (key, shuffle_results[key])))
    reduce_results = yield context.task_all(reduce_tasks)
    return reduce_results

main = df.Orchestrator.create(orchestrator_function)