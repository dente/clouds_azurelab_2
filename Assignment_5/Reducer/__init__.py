# This function is not intended to be invoked directly. Instead it will be
# triggered by an orchestrator function.
# Before running this sample, please:
# - create a Durable orchestration function
# - create a Durable HTTP starter function
# - add azure-functions-durable to requirements.txt
# - run pip install -r requirements.txt

import logging

'''
It receives a tuple in the form (key, [list of values])
'''
def main(params: str) -> str:
    word = params[0]
    values = params[1]
    return {word : sum(values)}
