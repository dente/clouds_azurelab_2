import logging

import azure.functions as func
import math
import json

def main(req: func.HttpRequest) -> func.HttpResponse:
    logging.info('Python HTTP trigger function processed a request.')
    lower = req.route_params.get('lower')
    upper = req.route_params.get('upper')
    return func.HttpResponse(numerical_integral(lower, upper))
        

def numerical_integration(lower, upper, N):
    total_area = 0
    width = (upper - lower) / N
    
    for i in range(N):
        x = lower + (i + 0.5) * width
        area = abs(math.sin(x)) * width
        total_area += area
    
    return total_area

def numerical_integral(lower, upper):
    lower = float(lower)
    upper = float(upper)
    results = []
    for N in [10, 100, 1000, 10000, 100000, 1000000]:
        results.append(numerical_integration(lower, upper, N))
    return json.dumps(results)
